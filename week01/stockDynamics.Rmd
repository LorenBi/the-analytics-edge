---
title: "Stock Dynamics"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
a=1+1
```

#Introduction

A stock market is where buyers and sellers trade shares of a company, and is one of the most popular ways for individuals and companies to invest money. The size of the world stock market  is now estimated to be in the trillions. The largest stock market in the world is the New York Stock Exchange (NYSE), located in New York City. About 2,800 companies are listed on the NSYE. In this problem, we'll look at the monthly stock prices of five of these companies: IBM, General Electric (GE), Procter and Gamble, Coca Cola, and Boeing. The data used in this problem comes from Infochimps.

Download and read the following files into R, using the read.csv function: IBMStock.csv, GEStock.csv, ProcterGambleStock.csv, CocaColaStock.csv, and BoeingStock.csv. (Do not open these files in any spreadsheet software before completing this problem because it might change the format of the Date field.)

Call the data frames "IBM", "GE", "ProcterGamble", "CocaCola", and "Boeing", respectively. Each data frame has two variables, described as follows:

* __Date:__ the date of the stock price, always given as the first of the month.
* __StockPrice:__ the average stock price of the company in the given month.
In this problem, we'll take a look at how the stock dynamics of these companies have changed over time.

```{r,include=FALSE}
#loading the data
IBM=read.csv("data/IBMStock.csv")
GE=read.csv("data/GEStock.csv")
ProcterGamble=read.csv("data/ProcterGambleStock.csv")
CocaCola=read.csv("data/CocaColaStock.csv")
Boeing=read.csv("data/BoeingStock.csv")
Companies=c("IBM", "GE", "ProcterGamble", "CocaCola", "Boeing")
```

# Summary Statistics
We first need to overwrite the data$Date with a date file
```{r}
IBM$Date = as.Date(IBM$Date, "%m/%d/%y")
GE$Date = as.Date(GE$Date, "%m/%d/%y")
CocaCola$Date = as.Date(CocaCola$Date, "%m/%d/%y")
ProcterGamble$Date = as.Date(ProcterGamble$Date, "%m/%d/%y")
Boeing$Date = as.Date(Boeing$Date, "%m/%d/%y")
```
The five datasets have the same number of observation
```{r}
str(IBM)
str(GE)
```
Now we can find:
* the earliest year in our datasets
* the latest
* the mean stock price of IBM
* the minimum stock price of GE
* the maximum stock price of CocaCola
* the median stock price of Boeing
* the standard dev of the stock price of P&G
```{r}
min(IBM$Date)
max(IBM$Date)
mean(IBM$StockPrice)
min(GE$StockPrice)
max(CocaCola$StockPrice)
median(Boeing$StockPrice)
sd(ProcterGamble$StockPrice)
```
Visualizing Stock Dynamics

We can see how the stock prices of CocaCola go with time compered to Procter & Gamble and highlight  the technology bubble burst of March of 2000
```{r}
plot(CocaCola$Date,CocaCola$StockPrice,type = "l", col="red",xlab="Date",ylab = "Stock price")
lines(ProcterGamble$Date, ProcterGamble$StockPrice, col="blue",lty=2)
abline(v=as.Date(c("2000-03-01")), lwd=1)
legend(as.Date(c("2001-03-01")),145,c("CocaCola","Procter & Gamble"),lty=c(1,2),lwd=c(1.5,1.5),col=c("red","blue"))
```
![alt text](http://i.imgur.com/5P1I3V9.png)
#Visualizing Stock Dynamics 1995-2005
We can see the dynamics over the decennial 1995-2005 of all the companies 
```{r}
plot(CocaCola$Date[301:432], CocaCola$StockPrice[301:432], type="l", col="red", ylim=c(0,210),xlab="Date",ylab = "Stock price")
lines(ProcterGamble$Date[301:432], ProcterGamble$StockPrice[301:432], col="blue",lty=2)
lines(GE$Date[301:432], GE$StockPrice[301:432], col="green",lty=3)
lines(IBM$Date[301:432],IBM$StockPrice[301:432], col="orange",lty=4)
lines(Boeing$Date[301:432],Boeing$StockPrice[301:432], col="purple",lty=5)
abline(v=as.Date(c("1997-10-01")), lwd=1)
legend(as.Date(c("2003-10-01")),210,c("CocaCola","Procter & Gamble","General Electronics","IBM","Boeing"),lty=c(1,2,3,4,5),lwd=c(1.5,1.5),col=c("red","blue","green","orange","purple"))
```
![alt text](http://i.imgur.com/N4YjEYR.png)
#Monthly Trends
We can compare the monthly average of the IBM stock price to the overall average. We can also see when the stock prices of GE and CocaCola peak or compare individual months
```{r}
tapply(IBM$StockPrice, months(IBM$Date), mean)>mean(IBM$StockPrice)
tapply(GE$StockPrice, months(GE$Date), mean)==max(tapply(GE$StockPrice, months(GE$Date), mean))
tapply(CocaCola$StockPrice, months(CocaCola$Date), mean)==max(tapply(CocaCola$StockPrice, months(CocaCola$Date), mean))
tapply(CocaCola$StockPrice, months(CocaCola$Date), mean)[months(as.Date("01-12-2000"))]<tapply(CocaCola$StockPrice, months(CocaCola$Date), mean)[months(as.Date("01-01-2000"))]
```

